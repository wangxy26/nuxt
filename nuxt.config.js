module.exports = {
  /*
   ** Headers of the page
   */
  head: {
    title: "test",
    meta: [{
        charset: "utf-8"
      },
      {
        name: "viewport",
        content: "width=device-width, initial-scale=1"
      },
      {
        hid: "description",
        name: "description",
        content: "Nuxt.js project"
      }
    ],
    link: [{
      rel: "icon",
      type: "image/x-icon",
      href: "./favicon.ico"
    }, ],
    // script: [
    //   {
    //     src: "/js/flexible.js",
    //     type: "text/javascript",
    //     charset: "utf-8"
    //   }
    // ]
  },
  /*
   ** Customize the progress bar color
   */
  loading: {
    color: "#3B8070"
  },
  plugins: [{
      src: "~/plugins/axios",
      //   src: "~plugins/spost",
      ssr: true
    },
    {
      src: "~plugins/element.js",
      ssr: true
    },
    // {
    //   src: "~plugins/spost",
    //   ssr: true
    // }
  ],
  modules: [
    // Doc: https://axios.nuxtjs.org/usage
    "@nuxtjs/axios"
    //"@nuxtjs/proxy"
  ],
  axios: {
    //是否允许跨域
    proxy: true,
    //最多重发三次
    retry: {
      retries: 3
    },
    //开发模式下开启debug
    debug: process.env._ENV == "production" ? false : true,
    //设置不同环境的请求地址
    baseURL: process.env._ENV == "production" ?
      "http://129.204.11.76:3000/api" : "http://129.204.11.76:3000/api",
    //是否是可信任
    withCredentials: true
  },
  proxy: {
    "/api": {
      target: "http://129.204.11.76:4000/api",
      changeOrigin: true,
      pathRewrite: {
        "^/api": ""
      }
    }
  },
  css: ["element-ui/lib/theme-chalk/index.css"],
  /*
   ** Build configuration
   */
  build: {
    /*
     ** Run ESLint on save
     */
    extend(config, {
      isDev,
      isClient
    }) {
      if (isDev && isClient) {
        config.module.rules.push({
          enforce: "pre",
          test: /\.(js|vue)$/,
          loader: "eslint-loader",
          exclude: /(node_modules)/
        });
      }
    }
    // postcss: [
    //   require("postcss-px2rem")({
    //     remUnit: 75
    //   })
    // ]
  }
};
