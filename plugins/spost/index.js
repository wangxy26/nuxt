import Vue from 'vue';
import AjaxMixin from './AjaxMixin';
import axios from 'axios'

Vue.mixin(AjaxMixin);
Vue.prototype.$axios = axios;
