const mysql = require('mysql');
const dbConfig = require('./db');
const sqlMap = require('./sqlMap');

const pool = mysql.createPool({
  host: dbConfig.mysql.host,
  user: dbConfig.mysql.user,
  password: dbConfig.mysql.password,
  database: dbConfig.mysql.database,
  port: dbConfig.mysql.port,
  multipleStatements: true // 多语句查询
});

module.exports = {
  login(req, res, next) {
    console.log(req.query)
    var name = req.query.name,
      pass = req.query.pass
    pool.getConnection((err, connection) => {
      var sql = sqlMap.login;
      connection.query(sql, [name, pass], (err, result) => {
        res.json(result);
        connection.release();
      })
    })
  },
  rejestername(req, res, next) {
    console.log(req.query)
    var name = req.query.name;
    pool.getConnection((err, connection) => {
      var sql = sqlMap.rejestername;
      connection.query(sql, [name], (err, result) => {
        res.json(result);
        connection.release();
      })
    })
  },
  rejesteremail(req, res, next) {
    console.log(req.query)
    var email = req.query.email;
    pool.getConnection((err, connection) => {
      var sql = sqlMap.rejesteremail;
      connection.query(sql, [email], (err, result) => {
        res.json(result);
        connection.release();
      })
    })
  },
  rejestertel(req, res, next) {
    console.log(req.query)
    var tel = req.query.tel;
    pool.getConnection((err, connection) => {
      var sql = sqlMap.rejestertel;
      connection.query(sql, [tel], (err, result) => {
        res.json(result);
        connection.release();
      })
    })
  },
  setValue(req, res, next) {
    console.log(req.body);
    var id = req.body.id,
      name = req.body.name,
      email = req.body.email;
    pool.getConnection((err, connection) => {
      var sql = sqlMap.setValue;
      connection.query(sql, [name, email, id], (err, result) => {
        res.json(result);
        connection.release();
      })
    })
  },
  delValue(req, res, next) {
    var id = req.body.id
    pool.getConnection((err, connection) => {
      var sql = sqlMap.delValue
      connection.query(sql, [id], (err, result) => {
        res.json(result);
        connection.release();
      })
    })
  },
  rejesterNew(req, res, next) {
    console.log(req.body)
    var pass = req.body.pass,
      name = req.body.name,
      email = req.body.email,
      tel = req.body.tel
    pool.getConnection((err, connection) => {
      var sql = sqlMap.rejesterNew
      connection.query(sql, [name, pass, email, tel], (err, result) => {
        res.json(result);
        connection.release();
      })
    })
  },
}
