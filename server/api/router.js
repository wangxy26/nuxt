const express = require('express');
const router = express.Router();
const api = require('./api');

router.get('/login', (req, res, next) => {
  api.login(req, res, next);
});

router.get('/rejestername', (req, res, next) => {
  api.rejestername(req, res, next);
});

router.get('/rejesteremail', (req, res, next) => {
  api.rejesteremail(req, res, next);
});

router.get('/rejestertel', (req, res, next) => {
  api.rejestertel(req, res, next);
});

router.post('/rejesterNew', (req, res, next) => {
  api.rejesterNew(req, res, next);
});

module.exports = router;
